import { HostConfigInterface } from "./host-config.interface";
export declare class HostConfig implements HostConfigInterface {
    readonly host: any;
    readonly port: number;
    constructor(host: any, port: string);
}
