import { AuthenticatedConnectionConfigInterface } from "./authenticated-connection-config.interface";
import { ConnectionConfig } from "./connection-config";
export declare class AuthenticatedConnectionConfig extends ConnectionConfig implements AuthenticatedConnectionConfigInterface {
    username: string;
    password: string;
    get connectionUrl(): string;
}
