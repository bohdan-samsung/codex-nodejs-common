export interface SqlHostConfigInterface {
    hostName: string;
    port: number;
    pool: {
        min: number;
        max: number;
    };
}
