"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.KafkaConfig = void 0;
const endpoint_config_1 = require("./endpoint-config");
const config_boolean_enum_1 = require("./config-boolean.enum");
class KafkaConfig extends endpoint_config_1.EndpointConfig {
    constructor(enabled, host, port, username, password, saslMechanism, topicsConfigPath, clientId, defaultPartitions) {
        super(host, port);
        this.host = host;
        this.username = username;
        this.password = password;
        this.topicsConfigPath = topicsConfigPath;
        this.clientId = clientId;
        this.saslMechanism = saslMechanism;
        this.enabled = enabled === config_boolean_enum_1.ConfigBooleanEnum.TRUE;
        this.defaultPartitions = defaultPartitions
            ? parseInt(defaultPartitions, 10)
            : 3;
    }
}
exports.KafkaConfig = KafkaConfig;
//# sourceMappingURL=kafka-config.js.map