import { ConnectionConfigInterface } from "./connection-config.interface";
import { EndpointConfig } from "./endpoint-config";
export declare class ConnectionConfig extends EndpointConfig implements ConnectionConfigInterface {
    protocol: string;
    get connectionUrl(): string;
}
