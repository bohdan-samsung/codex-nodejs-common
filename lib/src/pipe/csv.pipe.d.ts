import { ArgumentMetadata, PipeTransform } from "@nestjs/common";
export declare class CsvPipe implements PipeTransform {
    transform(value: any, metadata: ArgumentMetadata): any;
}
