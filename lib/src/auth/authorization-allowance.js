"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthorizationAllowance = void 0;
class AuthorizationAllowance {
    constructor(subject, object, objectId, action) {
        this.subject = subject;
        this.object = object;
        this.objectId = objectId;
        this.action = action;
    }
    isRequestAllowed(request) {
        if (request.object &&
            request.object.trim() !== "" &&
            this.object &&
            this.object.trim() !== "") {
            if (this.object !== "any" && request.object !== this.object) {
                return false;
            }
        }
        if (request.objectId &&
            request.objectId.toString().trim() !== "" &&
            this.objectId &&
            this.objectId.toString().trim() !== "") {
            if (this.objectId !== "any" &&
                request.objectId.toString() !== this.objectId.toString()) {
                return false;
            }
        }
        if (request.action &&
            request.action.trim() !== "" &&
            this.action &&
            this.action !== "") {
            if (this.action !== "any" && request.action !== this.action) {
                return false;
            }
        }
        return true;
    }
}
exports.AuthorizationAllowance = AuthorizationAllowance;
//# sourceMappingURL=authorization-allowance.js.map