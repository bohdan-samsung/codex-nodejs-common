"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpAuthzActionToSubObjectsGuardUtil = void 0;
const http_authz_guard_util_1 = require("./http-authz.guard.util");
class HttpAuthzActionToSubObjectsGuardUtil {
    constructor(context, action) {
        this.context = context;
        this.action = action;
        this._authzGuard = new http_authz_guard_util_1.HttpAuthzGuardUtil(context);
    }
    isAuthorized(object, objectId, subObject, subObjectIds, namespace) {
        for (const id of subObjectIds) {
            let requests = [];
            if (namespace) {
                requests = [
                    {
                        action: "",
                        object: namespace,
                        objectId: "",
                    },
                ];
            }
            requests = [
                ...requests,
                ...[
                    {
                        action: "",
                        object,
                        objectId,
                    },
                    {
                        action: this.action,
                        object: subObject,
                        objectId: id,
                    },
                ],
            ];
            if (!this._authzGuard.isAuthorized(...requests)) {
                return false;
            }
        }
        return true;
    }
    get params() {
        return this._authzGuard.params;
    }
    get query() {
        return this._authzGuard.query;
    }
    get body() {
        return this._authzGuard.body;
    }
}
exports.HttpAuthzActionToSubObjectsGuardUtil = HttpAuthzActionToSubObjectsGuardUtil;
//# sourceMappingURL=http-authz.action-to-sub-objects.guard.util.js.map