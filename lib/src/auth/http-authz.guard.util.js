"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpAuthzGuardUtil = void 0;
const common_1 = require("@nestjs/common");
const jwt = require("jsonwebtoken");
const authorization_allowance_1 = require("./authorization-allowance");
class HttpAuthzGuardUtil {
    constructor(context) {
        this.context = context;
        const request = context.switchToHttp().getRequest();
        const authorizationHeader = request.headers.authorization;
        if (!authorizationHeader) {
            throw new common_1.HttpException("Unauthorized", common_1.HttpStatus.UNAUTHORIZED);
        }
        const bearerTokenParts = authorizationHeader.trim().split("Bearer ");
        if (bearerTokenParts.length !== 2) {
            throw new common_1.HttpException("Unauthorized", common_1.HttpStatus.UNAUTHORIZED);
        }
        const bearerToken = bearerTokenParts[1];
        const decodedToken = jwt.decode(bearerToken);
        if (!decodedToken) {
            throw new common_1.HttpException("Unauthorized", common_1.HttpStatus.UNAUTHORIZED);
        }
        this._token = decodedToken;
        this.params = request.params;
        this.query = request.query;
        this.body = request.body;
    }
    isAuthorized(...authzRequests) {
        const scopes = this._token.scopes;
        for (const scope of scopes) {
            if (this._doesScopeAuthorizeRequest(scope, authzRequests)) {
                return true;
            }
        }
        return false;
    }
    _doesScopeAuthorizeRequest(scope, authzRequests) {
        const parts = scope.split(":");
        const authorizationAllowances = [];
        for (let i = 0; i < parts.length; i += 3) {
            authorizationAllowances.push(new authorization_allowance_1.AuthorizationAllowance(this._token.sub, parts[i], parts[1 + i], parts[2 + i]));
        }
        for (let i = 0; i < authzRequests.length; i++) {
            const request = authzRequests[i];
            if (!authorizationAllowances[i]) {
                return false;
            }
            const allowance = authorizationAllowances[i];
            if (!allowance.isRequestAllowed(request)) {
                return false;
            }
        }
        return true;
    }
}
exports.HttpAuthzGuardUtil = HttpAuthzGuardUtil;
//# sourceMappingURL=http-authz.guard.util.js.map