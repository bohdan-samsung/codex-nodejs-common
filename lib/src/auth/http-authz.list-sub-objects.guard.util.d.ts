import { ExecutionContext } from "@nestjs/common";
export declare class HttpAuthzListSubObjectsGuardUtil {
    private readonly context;
    private _util;
    constructor(context: ExecutionContext);
    isAuthorized(object: string, objectId: string, subObject: string, namespace?: string): boolean;
    get params(): any;
    get query(): any;
    get body(): any;
}
