"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpAuthzListSubObjectsGuardUtil = void 0;
const http_authz_guard_util_1 = require("./http-authz.guard.util");
class HttpAuthzListSubObjectsGuardUtil {
    constructor(context) {
        this.context = context;
        this._util = new http_authz_guard_util_1.HttpAuthzGuardUtil(context);
    }
    isAuthorized(object, objectId, subObject, namespace) {
        let requests = [];
        if (namespace) {
            requests = [
                {
                    action: "",
                    object: namespace,
                    objectId: "",
                },
            ];
        }
        requests = [
            ...requests,
            {
                action: "",
                object,
                objectId,
            },
            {
                action: "list",
                object: subObject,
                objectId: "",
            },
        ];
        return this._util.isAuthorized(...requests);
    }
    get params() {
        return this._util.params;
    }
    get query() {
        return this._util.query;
    }
    get body() {
        return this._util.body;
    }
}
exports.HttpAuthzListSubObjectsGuardUtil = HttpAuthzListSubObjectsGuardUtil;
//# sourceMappingURL=http-authz.list-sub-objects.guard.util.js.map