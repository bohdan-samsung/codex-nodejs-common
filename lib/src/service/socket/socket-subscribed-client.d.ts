import { TopicAuthorizorInterface } from "../../auth";
export declare class SocketSubscribedClient {
    private readonly token;
    private readonly client;
    private readonly authorizors;
    private topics;
    constructor(token: string, client: WebSocket, authorizors: TopicAuthorizorInterface[]);
    subscribeToTopics(topics: string[]): Promise<void>;
    getTopics(): (string | RegExp)[];
    private _isAuthorized;
    forwardFromTopic(topic: string, message: any): void;
}
