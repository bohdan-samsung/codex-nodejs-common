"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorHttpResponse = void 0;
const codex_data_model_1 = require("@cryptexlabs/codex-data-model");
const http_response_meta_1 = require("./http-response-meta");
class ErrorHttpResponse {
    constructor(status, locale, errorMessage, stack, config, correlationId, started) {
        this.errorMessage = errorMessage;
        this.stack = stack;
        this.meta = new http_response_meta_1.HttpResponseMeta(status, codex_data_model_1.CodexMetaTypeEnum.NA_HTTP_HEALTHZ, locale, config, correlationId, started);
    }
    toJSON() {
        return {
            meta: this.meta,
            data: this.data,
        };
    }
    get data() {
        return {
            message: this.errorMessage,
            stack: process.env.NODE_ENV !== "production" ? this.stack : null,
        };
    }
}
exports.ErrorHttpResponse = ErrorHttpResponse;
//# sourceMappingURL=error-http.response.js.map