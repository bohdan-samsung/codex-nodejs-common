import { HttpStatus } from "@nestjs/common";
import { LocaleInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";
export declare class HttpResponseMeta extends MessageMeta implements MessageMetaInterface {
    readonly status: HttpStatus;
    constructor(status: HttpStatus, type: string, locale: LocaleInterface, config: DefaultConfig, correlationId: string, started: Date);
    toJSON(): any;
}
