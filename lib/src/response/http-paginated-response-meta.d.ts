import { HttpStatus } from "@nestjs/common";
import { LocaleInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { DefaultConfig } from "../config";
import { MessageMeta } from "../message";
export declare class HttpPaginatedResponseMeta extends MessageMeta implements MessageMetaInterface {
    readonly status: HttpStatus;
    readonly totalRecords: number;
    constructor(status: HttpStatus, totalRecords: number, type: string, locale: LocaleInterface, config: DefaultConfig, correlationId: string, started: Date);
    toJSON(): any;
}
