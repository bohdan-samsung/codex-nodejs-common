import { HttpStatus } from "@nestjs/common";
import { MessageInterface, MessageMetaInterface } from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
export declare class RestResponse implements JsonSerializableInterface<MessageInterface<string>> {
    readonly data: any;
    readonly meta: MessageMetaInterface;
    constructor(context: Context, status: HttpStatus, type: string, data: any);
    toJSON(): MessageInterface<any>;
}
