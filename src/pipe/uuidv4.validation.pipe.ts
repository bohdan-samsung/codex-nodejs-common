import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import * as Joi from "joi";

@Injectable()
export class Uuidv4ValidationPipe implements PipeTransform {
  private _schema: Joi.Schema;
  constructor() {
    this._schema = Joi.string().uuid({ version: "uuidv4" });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
