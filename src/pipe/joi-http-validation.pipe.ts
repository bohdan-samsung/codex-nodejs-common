import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import { ObjectSchema } from "joi";

@Injectable()
export class JoiHttpValidationPipe implements PipeTransform {
  constructor(private schema: ObjectSchema) {}

  transform(value: any, metadata: ArgumentMetadata) {
    const validationResults = this.schema.validate(value);

    if (validationResults.error) {
      throw new HttpException(
        validationResults.error.message,
        HttpStatus.BAD_REQUEST
      );
    }

    return value;
  }
}
