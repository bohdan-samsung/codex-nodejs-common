import { LocaleUtil } from "./locale.util";
import { HttpException } from "@nestjs/common";
import { FriendlyHttpException } from "../exception";
import { ContextBuilder } from "../context";

describe("LocaleUtil", () => {
  describe("getLocaleFromHeaders", () => {
    it("Should throw error when no accept language header is specified", () => {
      expect(() => {
        LocaleUtil.getLocaleFromHeaders({}, null);
      }).toThrow(HttpException);
    });

    it("Should parse a normal header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "Accept-Language": "en-US",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse a lowercase header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-US",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an uppercase language code in header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "EN-US",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse a lowercase country code in header", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "en-us",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an accept language in the format: 'US,EN;Q=0.9'", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "US,EN;Q=0.9",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should parse an accept language in the format: 'en-US,en;q=0.9'", () => {
      const locale = LocaleUtil.getLocaleFromHeaders(
        {
          "accept-language": "    en-US,en;q=0.9",
        },
        null
      );
      expect(locale.language).toBe("en");
      expect(locale.country).toBe("US");
    });

    it("Should throw an error for an invalid country code", () => {
      expect(() => {
        LocaleUtil.getLocaleFromHeaders(
          { "accept-language": "asldkafsdj-*" },
          null
        );
      }).toThrow(FriendlyHttpException);
    });
  });
});
