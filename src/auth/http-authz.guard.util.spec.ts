import { ExecutionContext } from "@nestjs/common";
import { instance, mock, when } from "ts-mockito";
import { HttpArgumentsHost } from "@nestjs/common/interfaces/features/arguments-host.interface";
import * as jwt from "jsonwebtoken";
import { HttpAuthzGuardUtil } from "./http-authz.guard.util";

describe("HttpAuthzGuardUtil", () => {
  let mockedExecutionContext: ExecutionContext;
  let MockArgumentsHost;

  const getRequestWithAuthorizationBearerScopes = (
    sub: string,
    scopes: string[]
  ) => {
    const token = jwt.sign({ sub, scopes }, "asdf");
    const authorizationHeader = `Bearer ${token}`;
    return {
      headers: {
        authorization: authorizationHeader,
      },
    };
  };

  beforeEach(() => {
    const MockExecutionContext = mock<ExecutionContext>();
    mockedExecutionContext = instance(MockExecutionContext);
    MockArgumentsHost = mock<HttpArgumentsHost>();
    const mockedArgumentsHost = instance(MockArgumentsHost);
    when(MockExecutionContext.switchToHttp()).thenReturn(mockedArgumentsHost);
  });

  it("Should authorize a scope with object id", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:123:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "user",
        objectId: "123",
      })
    ).toBe(true);
  });

  it("Should authorize a scope with 'any' for object id", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:any:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "user",
        objectId: "123",
      })
    ).toBe(true);
  });

  it("Should authorize a request with number for id", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:123:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "user",
        objectId: 123,
      })
    ).toBe(true);
  });

  it("Should not authorize a scope with 'self' for object id", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:self:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "user",
        objectId: "johndoe",
      })
    ).toBe(false);
  });

  it("Should authorize a multi level scope definition", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "organization:5678::user:any:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized(
        {
          action: "",
          object: "organization",
          objectId: "5678",
        },
        {
          action: "update",
          object: "user",
          objectId: "123",
        }
      )
    ).toBe(true);
  });

  it("Should not authorize where action is not allowed", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:self:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "delete",
        object: "user",
        objectId: "johndoe",
      })
    ).toBe(false);
  });

  it("Should not authorize where object id does not match", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:123:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "user",
        objectId: "456",
      })
    ).toBe(false);
  });

  it("Should not authorize where object does not match", () => {
    const request = getRequestWithAuthorizationBearerScopes("johndoe", [
      "user:456:update",
    ]) as any;
    when(MockArgumentsHost.getRequest()).thenReturn(request);

    const util = new HttpAuthzGuardUtil(mockedExecutionContext);

    expect(
      util.isAuthorized({
        action: "update",
        object: "dog",
        objectId: "456",
      })
    ).toBe(false);
  });
});
