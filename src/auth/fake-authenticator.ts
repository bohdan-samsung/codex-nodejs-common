import { AuthenticatorInterface } from "./authenticator.interface";

export class FakeAuthenticator implements AuthenticatorInterface {
  authenticate(token: string): Promise<boolean> {
    return Promise.resolve(true);
  }
}
