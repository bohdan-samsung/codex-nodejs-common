import { ExecutionContext } from "@nestjs/common";
import { HttpAuthzGuardUtil } from "./http-authz.guard.util";

/**
 * Authorizes attachments of objects to another object by object id
 */
export class HttpAuthzActionToSubObjectsGuardUtil {
  private _authzGuard: HttpAuthzGuardUtil;

  constructor(
    private readonly context: ExecutionContext,
    private readonly action: string
  ) {
    this._authzGuard = new HttpAuthzGuardUtil(context);
  }

  /**
   * @param {string} object The object name of object A
   * @param {string} objectId The object ID of object A
   * @param {string} subObject The object name of objects B
   * @param {string[]} subObjectIds The object IDs of Objects B to attach to object A
   * @param {string?} namespace (Optional) The namespace of objects A and B
   */
  public isAuthorized(
    object: string,
    objectId: string,
    subObject: string,
    subObjectIds: string[],
    namespace?: string
  ) {
    for (const id of subObjectIds) {
      let requests = [];

      if (namespace) {
        requests = [
          {
            action: "",
            object: namespace,
            objectId: "",
          },
        ];
      }

      requests = [
        ...requests,
        ...[
          {
            action: "",
            object,
            objectId,
          },
          {
            action: this.action,
            object: subObject,
            objectId: id,
          },
        ],
      ];

      if (!this._authzGuard.isAuthorized(...requests)) {
        return false;
      }
    }
    return true;
  }

  public get params() {
    return this._authzGuard.params;
  }

  public get query() {
    return this._authzGuard.query;
  }

  public get body() {
    return this._authzGuard.body;
  }
}
