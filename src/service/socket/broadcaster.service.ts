import { WebsocketBroadcasterInterface } from "./websocket-broadcaster.interface";
import { SocketSubscribedClient } from "./socket-subscribed-client";

export class BroadcasterService implements WebsocketBroadcasterInterface {
  public subscribedClients: SocketSubscribedClient[] = [];

  public broadcast(topic, message: any) {
    const broadCastMessage = JSON.stringify(message);
    for (const wsClient of this.subscribedClients) {
      wsClient.forwardFromTopic(topic, broadCastMessage);
    }
  }

  public getAllTopicsForClients() {
    const topics = [];
    const topicToStrings = [];
    for (const client of this.subscribedClients) {
      const clientTopics = client.getTopics();
      for (const clientTopic of clientTopics) {
        if (!topicToStrings.includes(clientTopic.toString())) {
          topics.push(clientTopic);
          topicToStrings.push(clientTopic.toString());
        }
      }
    }

    return topics;
  }
}
