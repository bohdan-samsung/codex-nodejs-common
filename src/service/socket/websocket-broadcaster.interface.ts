export interface WebsocketBroadcasterInterface {
  broadcast(topic, message: any);
}
