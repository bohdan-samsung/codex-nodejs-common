import { Message } from "kafkajs";
import { EventHandlerInterface } from "./event.handler.interface";
import { LoggerService } from "@nestjs/common";

export class EventHandler {
  constructor(private readonly logger: LoggerService) {}

  public async handle(
    message: Message,
    topic: string,
    handler: EventHandlerInterface
  ) {
    let data;
    try {
      data = JSON.parse(message.value.toString());
      await handler.handle(data, topic);
    } catch (e) {
      // TODO publish message to error queue
      this.logger.error(
        `Error handling event: ${JSON.stringify(data)} from topic ${topic}`,
        e.stack
      );
    }
  }
}
