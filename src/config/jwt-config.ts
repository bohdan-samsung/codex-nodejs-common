import { JwtConfigInterface } from "./jwt-config.interface";

export class JwtConfig implements JwtConfigInterface {
  verify: boolean;

  constructor(jwtVerify: string, public readonly aud: string) {
    this.verify = jwtVerify === "true";
  }
}
