import { HttpStatus } from "@nestjs/common";
import {
  MessageInterface,
  MessageMetaInterface,
} from "@cryptexlabs/codex-data-model";
import { JsonSerializableInterface } from "../message";
import { Context } from "../context";
import { HttpResponseMeta } from "./http-response-meta";
import { HttpPaginatedResponseMeta } from "./http-paginated-response-meta";

export class RestPaginatedResponse
  implements JsonSerializableInterface<MessageInterface<string>>
{
  public readonly meta: MessageMetaInterface;

  constructor(
    context: Context,
    status: HttpStatus,
    totalRecords: number,
    type: string,
    public readonly data: any
  ) {
    this.meta = new HttpPaginatedResponseMeta(
      status,
      totalRecords,
      type,
      context.locale,
      context.config,
      context.correlationId,
      context.started
    ) as MessageMetaInterface;
  }

  public toJSON(): MessageInterface<any> {
    return {
      meta: this.meta,
      data: this.data,
    };
  }
}
